import { NgModule } from '@angular/core';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [LoginPageComponent, LoginFormComponent],
  imports: [SharedModule],
})
export class LoginModule {}
