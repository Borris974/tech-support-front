import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'ts-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Input() loading: boolean = false;
  @Output() login: EventEmitter<User> = new EventEmitter<User>();
  @Output() typing: EventEmitter<any> = new EventEmitter<any>();
  public hide: boolean = true;
  public user: User = new User();
  public loginForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  });

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit(): void {}

  onLogin() {
    const user = {
      email: this.email,
      password: this.password,
    };
    this.login.emit(user);
  }

  onTyping() {
    this.typing.emit();
  }

  get email(): string {
    return this.loginForm && this.loginForm.get('email')?.value;
  }
  get password(): string {
    return this.loginForm && this.loginForm.get('password')?.value;
  }
}
