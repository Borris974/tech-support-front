import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'ts-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  public loading: boolean = false;

  constructor(private readonly authService: AuthService) {}

  ngOnInit(): void {}

  login(userCredentials: any) {
    this.loading = true;
    this.authService
      .signin(userCredentials)
      .subscribe({
        next: (response) => {
        },
        error: (error: any) => {
          console.error(error);
        },
      })
      .add(() => {
        this.loading = false;
      });
  }
}
