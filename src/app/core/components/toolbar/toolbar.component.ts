import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ts-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() drawer:any;

  constructor() { }

  ngOnInit(): void {
  }

}
