import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PublicModule } from '../public/public.module';
import { DeveloperModule } from '../developer/developer.module';
import { AdminModule } from '../admin/admin.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SharedModule } from '../shared/shared.module';
import { AuthInterceptor } from './interceptors/auth.interceptor';

@NgModule({
  declarations: [PageNotFoundComponent, ToolbarComponent],
  imports: [
    HttpClientModule,
    SharedModule,
    PublicModule,
    DeveloperModule,
    AdminModule,
  ],
  exports: [ToolbarComponent, PageNotFoundComponent],

  providers: [
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
})
export class CoreModule {}
