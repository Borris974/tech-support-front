import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly http: HttpClient) {}

  public getUsers(params = {}): Observable<Array<User>> {
    return this.http
      .get<Array<User>>(`${environment.api}/users/`, {
        params: { ...params },
      })
      .pipe(debounceTime(300));
  }

  public getUser(userId: string): Observable<User> {
    return this.http
      .get<User>(`${environment.api}/users/${userId}`)
      .pipe(debounceTime(300));
  }

  public createUser(user: User): Observable<User> {
    return this.http
      .post<User>(`${environment.api}/users/`, user)
      .pipe(debounceTime(300));
  }

  public deleteUser(userId: string): Observable<any> {
    return this.http
      .delete(`${environment.api}/users/${userId}`)
      .pipe(debounceTime(300));
  }


}
