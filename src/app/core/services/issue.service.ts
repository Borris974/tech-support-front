import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Issue } from 'src/app/shared/models/issue.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class IssueService {
  constructor(private readonly http: HttpClient) {}

  public getStats(): Observable<any> {
    return this.http
      .get(`${environment.api}/issues/stats`)
      .pipe(debounceTime(300));
  }

  public getIssues(params = {}): Observable<Array<Issue>> {
    return this.http
      .get<Array<Issue>>(`${environment.api}/issues/`, {
        params: { ...params },
      })
      .pipe(debounceTime(300));
  }

  public getIssue(issueId: string): Observable<Issue> {
    return this.http
      .get<Issue>(`${environment.api}/issues/${issueId}`)
      .pipe(debounceTime(300));
  }

  public createIssue(
    currentUserId: string,
    issue: FormData
  ): Observable<Issue> {
    return this.http
      .post<Issue>(`${environment.api}/users/${currentUserId}/issues`, issue)
      .pipe(debounceTime(300));
  }

  public updateIssue(issueId: string, issue: FormData): Observable<any> {
    return this.http
      .patch(`${environment.api}/issues/${issueId}`, issue)
      .pipe(debounceTime(300));
  }

  public deleteIssue(issueId: string): Observable<any> {
    return this.http
      .delete(`${environment.api}/issues/${issueId}`)
      .pipe(debounceTime(300));
  }
}
