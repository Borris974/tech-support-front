import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { debounceTime, map, tap } from 'rxjs/operators';
import { UserRole } from 'src/app/shared/enums/user-role.enum';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userConnected$ = new BehaviorSubject<User | null>(null);
  get currentUser(): User | null {
    return this.userConnected$.getValue();
  }

  redirectUrl: string = 'app/dashboard';

  constructor(private router: Router, private http: HttpClient) {}

  signin(credentials: User): Observable<any> {
    const data = {
      email: credentials.email,
      password: credentials.password,
    };

    return this.http.post(`${environment.api}/auth/signin`, data).pipe(
      tap((response: any) => {
        if (response) {
          localStorage.clear();
          localStorage.setItem(
            'accessToken',
            JSON.stringify(response.accessToken)
          );
          this.userConnected$.next(response.user as User);

          switch (response.user.role) {
            case UserRole.DEVELOPER:
              this.redirectUrl = 'app/issues';
              break;
            case UserRole.MANAGER:
              this.redirectUrl = 'mng/dashboard';
              break;
            case UserRole.ADMIN:
              this.redirectUrl = 'adm/dashboard';
              break;
            default:
              this.redirectUrl = 'app/dashboard';
          }

          this.router.navigate([this.redirectUrl]);
        } else {
          this.signout();
        }
      })
    );
  }

  isAccessTokenSet(): boolean {
    return !!localStorage.getItem('accessToken');
  }

  isLoggedIn(): boolean {
    return !!this.isAccessTokenSet();
  }

  getAccessToken(): string {
    return (
      this.isAccessTokenSet() &&
      JSON.parse(localStorage.getItem('accessToken') ?? '')
    );
  }

  me(): Observable<User> {
    return this.http.get(`${environment.api}/auth/me/`).pipe(
      debounceTime(300),
      tap((response:User) => {
        this.userConnected$.next(response);
      })
    );
  }

  signout(): void {
    localStorage.removeItem('accessToken');
    localStorage.clear();
    this.userConnected$.next(null);
    this.router.navigate(['login']);
  }
}
