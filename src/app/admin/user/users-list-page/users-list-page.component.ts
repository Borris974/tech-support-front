import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'ts-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.scss'],
})
export class UsersListPageComponent implements OnInit {
  public currentUser: User | null = null;
  public users: Array<User> = [];
  public loading: boolean = true;
  public displayedColumns: Array<string> = ['id', 'name', 'email', 'role', 'actions'];

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authService.currentUser;
    this.getData();
  }

  getData() {
    this.loading = true;
    this.userService
      .getUsers()
      .subscribe({
        next: (response: Array<User>) => {
          this.users = response;
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => (this.loading = false));
  }
  navigateToCreate() {
    this.router.navigate(['adm/users/create']);
  }

  navigateToShow(userId: string): void {
    this.router.navigate([`adm/users/${userId}`]);
  }
  navigateToUpdate(issueId: string): void {
    this.router.navigate([`adm/users/${issueId}/update`]);
  }

  deleteIssue(issueId: string): void {
    this.userService.deleteUser(issueId).subscribe({
      next: (response) => {
        this.getData();
      },
      error: (error) => {
        console.error(error);
      },
    });
  }
}
