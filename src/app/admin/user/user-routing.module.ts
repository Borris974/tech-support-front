import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUserPageComponent } from './create-user-page/create-user-page.component';
import { UsersListPageComponent } from './users-list-page/users-list-page.component';

const routes: Routes = [
  {
    path: '',
    children: [{ path: '', component: UsersListPageComponent }],
  },
  {
    path: 'create',
    component: CreateUserPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
