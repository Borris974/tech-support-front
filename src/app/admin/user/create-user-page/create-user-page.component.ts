import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'ts-create-user-page',
  templateUrl: './create-user-page.component.html',
  styleUrls: ['./create-user-page.component.scss'],
})
export class CreateUserPageComponent implements OnInit {
  private currentUser: User | null = this.authService.currentUser;
  public loading: boolean = false;
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {}

  create(user: User) {
    this.loading = true;

    this.userService
      .createUser(user)
      .subscribe({
        next: (response) => {
          this.loading = false;
          this.router.navigate(['adm/users/']);
        },
        error: (error) => {
          this.loading = false;
          console.error(error);
        },
      })
      .add(() => (this.loading = false));
  }
}
