import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { IssueService } from 'src/app/core/services/issue.service';
import { Issue } from 'src/app/shared/models/issue.model';

@Component({
  selector: 'ts-update-issue-page',
  templateUrl: './update-issue-page.component.html',
  styleUrls: ['./update-issue-page.component.scss'],
})
export class UpdateIssuePageComponent implements OnInit {
  issueId: string = this.route.snapshot.params.id;
  issue$: BehaviorSubject<Issue | null> = new BehaviorSubject<Issue | null>(null);
  loading: boolean = true;
  saveLoading: boolean = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly issueService: IssueService
  ) {}

  ngOnInit(): void {
    this.getIssue();
  }

  getIssue() {
    this.loading = true;
    this.issueService
      .getIssue(this.issueId)
      .subscribe({
        next: (response: Issue) => {
          this.issue$?.next(response);
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => {
        this.loading = false;
      });
  }

  update(issueWrapper: { issue: Issue; files?: Array<File> }): void {
    const formData = new FormData();
    formData.append('title', issueWrapper?.issue?.title!);
    formData.append('content', issueWrapper?.issue?.content!);

    issueWrapper.files?.forEach((file: File) => {
      formData.append('files', file);
    });

    this.issueService
      .updateIssue(this.issueId, formData)
      .subscribe({
        next: (response: Issue) => {
          this.router.navigate([`adm/issues/${this.issueId}`]);
        },
        error: (error: any) => {
          console.error(error);
        },
      })
      .add(() => {
        this.loading = false;
      });
  }
}
