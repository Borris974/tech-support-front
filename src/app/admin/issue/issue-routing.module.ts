import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateIssuePageComponent } from './create-issue-page/create-issue-page.component';
import { IssuesListPageComponent } from './issues-list-page/issues-list-page.component';
import { ShowIssuePageComponent } from './show-issue-page/show-issue-page.component';
import { UpdateIssuePageComponent } from './update-issue-page/update-issue-page.component';

const routes: Routes = [
  {
    path: '',
    children: [{ path: '', component: IssuesListPageComponent }],
  },
  {
    path: 'create',
    component: CreateIssuePageComponent
  },
  {
    path: ':id',
    children: [
      { path: '', component: ShowIssuePageComponent },
      { path: 'update', component: UpdateIssuePageComponent },
    ],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IssueRoutingModule {}
