import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IssueService } from 'src/app/core/services/issue.service';
import { Issue } from 'src/app/shared/models/issue.model';

@Component({
  selector: 'ts-show-issue-page',
  templateUrl: './show-issue-page.component.html',
  styleUrls: ['./show-issue-page.component.scss'],
})
export class ShowIssuePageComponent implements OnInit {
  private issueId: string = this.route.snapshot.params.id;
  public issue?: Issue;
  public loading: boolean = true;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly issueService: IssueService
  ) {}

  ngOnInit(): void {
    this.getIssue(this.issueId);
  }

  getIssue(id: string): void {
    this.loading = true;
    this.issueService
      .getIssue(id)
      .subscribe({
        next: (response: Issue) => {
          this.issue = response;
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => {
        this.loading = false;
      });
  }
}
