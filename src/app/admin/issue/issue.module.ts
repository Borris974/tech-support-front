import { NgModule } from '@angular/core';
import { IssuesListPageComponent } from './issues-list-page/issues-list-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { IssueRoutingModule } from './issue-routing.module';
import { CreateIssuePageComponent } from './create-issue-page/create-issue-page.component';
import { ShowIssuePageComponent } from './show-issue-page/show-issue-page.component';
import { UpdateIssuePageComponent } from './update-issue-page/update-issue-page.component';



@NgModule({
  declarations: [
    IssuesListPageComponent,
    CreateIssuePageComponent,
    ShowIssuePageComponent,
    UpdateIssuePageComponent
  ],
  imports: [
    SharedModule,
    IssueRoutingModule
  ]
})
export class IssueModule { }
