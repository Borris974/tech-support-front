import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { IssueService } from 'src/app/core/services/issue.service';
import { Issue } from 'src/app/shared/models/issue.model';

@Component({
  selector: 'ts-create-issue-page',
  templateUrl: './create-issue-page.component.html',
  styleUrls: ['./create-issue-page.component.scss'],
})
export class CreateIssuePageComponent implements OnInit {
  private currentUser = this.authService.currentUser;
  public loading: boolean = false;
  constructor(
    private readonly authService: AuthService,
    private readonly issueService: IssueService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {}

  create(issueWrapper: {issue:Issue, files?: Array<File>}) {
    this.loading = true;
    const formData = new FormData();
    formData.append('title', issueWrapper?.issue?.title!);
    formData.append('content', issueWrapper?.issue?.content!);

    issueWrapper.files?.forEach((file:File) => {
      formData.append('files', file)
    });


    this.issueService
      .createIssue(this.currentUser?.id!, formData)
      .subscribe({
        next: (response) => {
          this.router.navigate(['adm/issues/']);
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => (this.loading = false));
  }
}
