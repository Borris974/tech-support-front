import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ts-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  @Input() stats?:any;

  constructor() { }

  ngOnInit(): void {
  }

}
