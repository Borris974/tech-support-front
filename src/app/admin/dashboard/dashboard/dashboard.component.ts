import { Component, OnInit } from '@angular/core';
import { IssueService } from 'src/app/core/services/issue.service';

@Component({
  selector: 'ts-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  constructor() {}

  ngOnInit(): void {

  }
}
