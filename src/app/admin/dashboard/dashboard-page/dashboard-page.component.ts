import { Component, OnInit } from '@angular/core';
import { IssueService } from 'src/app/core/services/issue.service';

@Component({
  selector: 'ts-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit {
  public stats: any;
  public loading: boolean = true;
  constructor(private readonly issueService: IssueService) {}

  ngOnInit(): void {
    this.loading = true;
    this.issueService
      .getStats()
      .subscribe({
        next: (response) => {
          this.stats = response;
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => {
        this.loading = false;
      });
  }
}
