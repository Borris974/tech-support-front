import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { RoleGuard } from '../core/guards/role.guard';
import { UserRole } from '../shared/enums/user-role.enum';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';

const routes: Routes = [
  {
    path: 'app',
    canActivate: [AuthGuard, RoleGuard],
    data: { roles: [UserRole.DEVELOPER] },
    children: [
      // {
      //   path: 'dashboard',
      //   component: DashboardPageComponent,
      // },
      {
        path: 'issues',
        loadChildren: () =>
          import('./issue/issue.module').then((m) => m.IssueModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeveloperRoutingModule {}
