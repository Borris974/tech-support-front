import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { IssueRoutingModule } from './issue-routing.module';
import { IssuesListPageComponent } from './issues-list-page/issues-list-page.component';
import { ShowIssuePageComponent } from './show-issue-page/show-issue-page.component';

@NgModule({
  declarations: [
    IssuesListPageComponent,
    ShowIssuePageComponent
  ],
  imports: [SharedModule, IssueRoutingModule],
})
export class IssueModule {}
