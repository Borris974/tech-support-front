import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IssuesListPageComponent } from './issues-list-page/issues-list-page.component';
import { ShowIssuePageComponent } from './show-issue-page/show-issue-page.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: IssuesListPageComponent },
      {
        path: ':id',
        children: [{ path: '', component: ShowIssuePageComponent }],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IssueRoutingModule {}
