import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { IssueService } from 'src/app/core/services/issue.service';
import { Issue } from 'src/app/shared/models/issue.model';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'ts-issues-list-page',
  templateUrl: './issues-list-page.component.html',
  styleUrls: ['./issues-list-page.component.scss']
})
export class IssuesListPageComponent implements OnInit {

  public refSearchTerm$: Subject<string> = new Subject<string>();
  public titleSearchTerm$: Subject<string> = new Subject<string>();
  public contentSearchTerm$: Subject<string> = new Subject<string>();
  public creatorSearchTerm$: Subject<string> = new Subject<string>();

  public currentUser: User | null = null;
  public issues: Array<Issue> = [];
  public loading: boolean = true;
  public displayedColumns: Array<string> = [
    'ref',
    'title',
    'content',
    'attachments',
    'actions',
  ];
  private searchOptions: any = {};

  constructor(
    private readonly authService: AuthService,
    private readonly issueService: IssueService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authService.currentUser;
    this.getData();

    this.refSearchTerm$
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (response) => {
          this.searchOptions.ref = response ?? '';
          this.getData();
        },
      });

    this.titleSearchTerm$
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (response) => {
          this.searchOptions.title = response ?? '';
          this.getData();
        },
      });

    this.contentSearchTerm$
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (response) => {
          this.searchOptions.content = response ?? '';
          this.getData();
        },
      });

    this.creatorSearchTerm$
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe({
        next: (response) => {
          this.searchOptions.creator = response ?? '';
          this.getData();
        },
      });
  }

  getData() {
    this.loading = true;
    this.issueService
      .getIssues(this.searchOptions)
      .subscribe({
        next: (response: Array<Issue>) => {
          this.issues = response;
        },
        error: (error) => {
          console.error(error);
        },
      })
      .add(() => (this.loading = false));
  }

  refFilterChanged(term: string) {
    this.refSearchTerm$.next(term);
  }

  titleFilterChanged(term: string) {
    this.titleSearchTerm$.next(term);
  }

  contentFilterChanged(term: string) {
    this.contentSearchTerm$.next(term);
  }

  creatorFilterChanged(term: string) {
    this.creatorSearchTerm$.next(term);
  }

  navigateToShow(issueId: string): void {
    this.router.navigate([`app/issues/${issueId}`]);
  }
  navigateToUpdate(issueId: string): void {
    this.router.navigate([`app/issues/${issueId}/update`]);
  }

}
