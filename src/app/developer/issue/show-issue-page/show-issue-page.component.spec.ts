import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIssuePageComponent } from './show-issue-page.component';

describe('ShowIssuePageComponent', () => {
  let component: ShowIssuePageComponent;
  let fixture: ComponentFixture<ShowIssuePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowIssuePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIssuePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
