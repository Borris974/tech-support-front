import { NgModule } from '@angular/core';
import { DeveloperRoutingModule } from './developer-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';


@NgModule({
  declarations: [
    DashboardPageComponent
  ],
  imports: [
    SharedModule,
    DeveloperRoutingModule
  ]
})
export class DeveloperModule { }
