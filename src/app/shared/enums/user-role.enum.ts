export enum UserRole {
  DEVELOPER = 'developer',
  MANAGER = 'manager',
  ADMIN = 'admin',
}

