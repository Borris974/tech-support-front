import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserRole } from '../../enums/user-role.enum';
import { User } from '../../models/user.model';

@Component({
  selector: 'ts-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  public UserRole = UserRole;
  public saveUserForm: FormGroup = this.formBuilder.group({
    firstname: [undefined, [Validators.required]],
    lastname: [undefined, [Validators.required]],
    email: [undefined, [Validators.required, Validators.email]],
    password: [undefined,
      [Validators.required, Validators.minLength(8),
        this.regexValidator(new RegExp(/(?=.*[A-Z].*[A-Z]).*$/), {'min_uppercase': 'min_uppercase'}),
        this.regexValidator(new RegExp(/(?=.*[!@#$&*]).*$/), {'min_special_char': 'min_special_char'}),
        this.regexValidator(new RegExp(/(?=.*[0-9].*[0-9]).*$/), {'min_number': 'minmaj'}),
        this.regexValidator(new RegExp(/(?=.*[a-z].*[a-z].*[a-z]).*$/), {'min_lowcase': 'min_lowcase'}),
    ]],
    role: [UserRole.DEVELOPER, [Validators.required]],
  });
  @Input() mode: 'create' | 'update' = 'create';
  @Input() loading: boolean = false;
  @Input() user$?: Observable<User | null>;
  @Input() user?: User | null;
  @Output() save: EventEmitter<User> = new EventEmitter<User>();

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {}

  regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }

  submit() {
    this.save.emit({
      id: this.user?.id ?? undefined,
      firstname: this.firstname,
      lastname: this.lastname,
      email: this.email,
      password: this.password,
      role: this.role
    } as User);
  }

  get firstname() {
    if (this.saveUserForm.get('firstname')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveUserForm.get('firstname')?.value?.trim();
  }

  set firstname(value: string | undefined) {
    this.saveUserForm.get('firstname')?.setValue(value);
  }

  get lastname() {
    if (this.saveUserForm.get('lastname')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveUserForm.get('lastname')?.value?.trim();
  }

  set lastname(value: string | undefined) {
    this.saveUserForm.get('lastname')?.setValue(value);
  }

  get role() {
    if (this.saveUserForm.get('role')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveUserForm.get('role')?.value?.trim();
  }

  set role(value: string | undefined) {
    this.saveUserForm.get('role')?.setValue(value);
  }

  get email() {
    if (this.saveUserForm.get('email')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveUserForm.get('email')?.value?.trim();
  }

  set email(value: string | undefined) {
    this.saveUserForm.get('email')?.setValue(value);
  }

  get password() {
    if (this.saveUserForm.get('password')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveUserForm.get('password')?.value?.trim();
  }

  set password(value: string | undefined) {
    this.saveUserForm.get('password')?.setValue(value);
  }
}
