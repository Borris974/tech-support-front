import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserRole } from '../../enums/user-role.enum';
import { User } from '../../models/user.model';

@Component({
  selector: 'ts-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  @Input() displayedColumns: Array<string> = [];
  @Input() currentUser?: User | null;
  @Input() users: Array<User> = new Array<User>();
  @Input() loading: boolean = false;

  @Output() show: EventEmitter<string> = new EventEmitter<string>();
  @Output() delete: EventEmitter<string> = new EventEmitter<string>();
  @Output() update: EventEmitter<string> = new EventEmitter<string>();

  public UserRole = UserRole;

  constructor() {}

  ngOnInit(): void {}

  onUpdate(id: string) {
    this.update.emit(id);
  }

  onShow(id: string) {
    this.show.emit(id);
  }

  onDelete(element: User) {
    this.delete.emit(element.id);
  }
}
