import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Issue } from '../../models/issue.model';

@Component({
  selector: 'ts-show-issue',
  templateUrl: './show-issue.component.html',
  styleUrls: ['./show-issue.component.scss']
})
export class ShowIssueComponent implements OnInit {
  public attachmentsUrl: string = environment.attachmentsUrl;
  @Input() issue?: Issue;

  constructor() { }

  ngOnInit(): void {
  }

}
