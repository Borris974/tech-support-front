import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserRole } from '../../enums/user-role.enum';
import { Issue } from '../../models/issue.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'ts-issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.scss'],
})
export class IssuesListComponent implements OnInit {
  @Input() displayedColumns: Array<string> = [];
  @Input() currentUser?: User | null;
  @Input() issues: Array<Issue> = new Array<Issue>();
  @Input() loading: boolean = false;
  @Output() refFilterChanged: EventEmitter<string> =
    new EventEmitter<string>();
  @Output() titleFilterChanged: EventEmitter<string> =
    new EventEmitter<string>();
  @Output() contentFilterChanged: EventEmitter<string> =
    new EventEmitter<string>();
  @Output() creatorFilterChanged: EventEmitter<string> =
    new EventEmitter<string>();
  @Output() show: EventEmitter<string> = new EventEmitter<string>();
  @Output() delete: EventEmitter<string> = new EventEmitter<string>();
  @Output() update: EventEmitter<string> = new EventEmitter<string>();

  public UserRole = UserRole;

  constructor() {}

  ngOnInit(): void {}

  onRefFilterChanged(term: string) {
    this.refFilterChanged.emit(term);
  }
  onTitleFilterChanged(term: string) {
    this.titleFilterChanged.emit(term);
  }

  onContentFilterChanged(term: string) {
    this.contentFilterChanged.emit(term);
  }

  onCreatorFilterChanged(term: string) {
    this.creatorFilterChanged.emit(term);
  }

  onUpdate(id: string) {
    this.update.emit(id);
  }

  onShow(id: string) {
    this.show.emit(id);
  }

  onDelete(element: Issue) {
    this.delete.emit(element.id);
  }
}
