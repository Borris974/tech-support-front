import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { Issue } from '../../models/issue.model';

@Component({
  selector: 'ts-issue-form',
  templateUrl: './issue-form.component.html',
  styleUrls: ['./issue-form.component.scss'],
})
export class IssueFormComponent implements OnInit {
  public saveIssueForm: FormGroup = this.formBuilder.group({
    title: [undefined, [Validators.required]],
    content: [undefined, [Validators.required]],
  });
  @Input() mode: 'create' | 'update' = 'create';
  @Input() loading: boolean = false;
  @Input() issue$?: Observable<Issue | null>;
  @Input() issue?: Issue | null;
  @Output() save: EventEmitter<{ issue: Issue; files?: Array<File> }> =
    new EventEmitter<{ issue: Issue; files?: Array<File> }>();
  private fileList?: FileList | null;

  constructor(private readonly formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.issue$?.subscribe({
      next: (response: Issue | null) => {
        this.issue = response;
        this.title = this.issue?.title;
        this.content = this.issue?.content;
      },
    });
  }

  submit() {
    let files;

    if (this.fileList) {
      files = Array.from(this.fileList);
    }

    this.save.emit({
      issue: {
        id: this.issue?.id ?? undefined,
        title: this.title,
        content: this.content,
      } as Issue,
      files,
    });
  }

  handleFiles(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    if (element?.files?.length! <= 3) {
      this.fileList = element.files;
    } else {
      alert('Vous pouvez uploader 3 fichiers maximum');
      console.error('3 fichiers max !');
    }
  }

  get title() {
    if (this.saveIssueForm.get('title')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveIssueForm.get('title')?.value?.trim();
  }

  set title(value: string | undefined) {
    this.saveIssueForm.get('title')?.setValue(value);
  }

  get content() {
    if (this.saveIssueForm.get('content')?.value?.trim() === '') {
      return undefined;
    }
    return this.saveIssueForm.get('content')?.value?.trim();
  }

  set content(value: string | undefined) {
    this.saveIssueForm.get('content')?.setValue(value);
  }
}
