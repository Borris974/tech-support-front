import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatBadgeModule } from '@angular/material/badge';
import { MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { IssuesListComponent } from './components/issues-list/issues-list.component';
import { IssueFormComponent } from './components/issue-form/issue-form.component';
import { ShowIssueComponent } from './components/show-issue/show-issue.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UserFormComponent } from './components/user-form/user-form.component';

const AngularMaterialModules = [
  FlexLayoutModule,
  MatDialogModule,
  MatToolbarModule,
  MatMenuModule,
  MatSnackBarModule,
  MatDividerModule,
  MatBadgeModule,
  MatRippleModule,
  MatProgressSpinnerModule,
  MatTableModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatBottomSheetModule,
  MatDatepickerModule,
  MatProgressBarModule,
  MatGridListModule,
  MatListModule,
  MatChipsModule,
  MatPaginatorModule,
  MatSelectModule,
  MatTooltipModule,
];

@NgModule({
  declarations: [
    IssuesListComponent,
    IssueFormComponent,
    ShowIssueComponent,
    UsersListComponent,
    UserFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...AngularMaterialModules,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ...AngularMaterialModules,
    IssuesListComponent,
    IssueFormComponent,
    ShowIssueComponent,
    UsersListComponent,
    UserFormComponent,
  ],
  entryComponents: [],
})
export class SharedModule {}
