import { UserRole } from '../enums/user-role.enum';
import { Issue } from './issue.model';

export class User {
  readonly id?: string | undefined;
  email?: string | undefined;
  password?: string | undefined;
  firstname?: string | undefined;
  lastname?: string | undefined;
  role?: UserRole | undefined;
  assignedIssues?: Array<Issue> | undefined;
  createdIssues?: Array<Issue> | undefined;

  constructor(
    params: {
      id?: string;
      email?: string;
      password?: string;
      firstname?: string;
      lastname?: string;
      role?: UserRole;
      assignedIssues?: Array<Issue>;
      createdIssues?: Array<Issue>;
    } = {}
  ) {
    this.id = params.id;
    this.email = params.email;
    this.password = params.password;
    this.firstname = params.firstname;
    this.lastname = params.lastname;
    this.role = params.role;
    this.assignedIssues = params.assignedIssues;
    this.createdIssues = params.createdIssues;
  }
}
