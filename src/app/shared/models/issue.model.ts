import { IssueStatus } from '../enums/issue-status.enum';
import { User } from './user.model';

export class Issue {
  readonly id?: string | undefined;
  createdAt?: Date | string | undefined;
  updatedAt?: Date | string | undefined;
  ref?: string | undefined;
  title?: string | undefined;
  content?: string | undefined;
  status?: IssueStatus | undefined;
  attachments?: Array<string> | undefined;
  creator?: User | undefined;
  assignees?: Array<User> | undefined;

  constructor(
    params: {
      id?: string;
      createdAt?: Date | string;
      updatedAt?: Date | string;
      ref?: string;
      title?: string;
      content?: string;
      status?: IssueStatus;
      attachments?: Array<string>;
      creator?: User;
      assignees?: Array<User>;
    } = {}
  ) {
    this.id = params.id;
    this.createdAt = params.createdAt;
    this.updatedAt = params.updatedAt;
    this.ref = params.ref;
    this.title = params.title;
    this.content = params.content;
    this.status = params.status;
    this.attachments = params.attachments;
    this.creator = params.creator;
    this.assignees = params.assignees;
  }
}
