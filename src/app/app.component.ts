import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './core/services/auth.service';
import { UserRole } from './shared/enums/user-role.enum';
import { User } from './shared/models/user.model';

@Component({
  selector: 'ts-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'tech-support-front';
  opened: boolean = true;
  public currentUser?: User | null;
  public UserRole = UserRole;

  protectedNavItems: Array<any> = [
  ];

  developerNavItems: Array<any> = [
    {
      name: 'Tickets',
      link: '/app/issues',
      icon: 'list',
    },
  ];

  managerNavItems: Array<any> = [
    {
      name: 'Dashbord MANAGER',
      link: '/mng/dashboard',
      icon: 'list',
    },
    {
      name: 'Tickets',
      link: '/mng/issues',
      icon: 'list',
    },
  ];

  adminNavItems: Array<any> = [
    {
      name: 'Dashbord ADMIN',
      link: '/adm/dashboard',
      icon: 'list',
    },
    {
      name: 'Tickets',
      link: '/adm/issues',
      icon: 'list',
    },
    {
      name: 'Utilisateurs',
      link: '/adm/users',
      icon: 'list',
    },
  ];

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}
  ngOnInit(): void {
    this.authService.me().subscribe();
    this.authService.userConnected$.subscribe({
      next: (response: User | null) => {
        this.currentUser = response;
      },
      error: (error) => console.error(error),
    });
  }

  navigate(link: string) {
    this.router.navigate([link]);
  }

  navigateToLogin() {
    this.router.navigate(['/login']);
  }

  logout() {
    this.authService.signout();
  }
}
