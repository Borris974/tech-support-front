# Stage 1
FROM node:14 as install-node-step

RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN npm install -g npm@8.1.1

RUN npm install --loglevel verbose
RUN npm run build

# Stage 2
FROM nginx:1.17.1-alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=install-node-step /app/dist/tech-support-front /usr/share/nginx/html

